import { Outlet, Route, Routes, useNavigate } from "react-router-dom";
import "./App.css";
import IframeComponent from "./components/Iframe.jsx";

import Navigation from "./components/Navigation/Navigation.jsx";
import MomentDurationCalendar from "./components/Examples/momentjs/Duration/MomentDurationCalendar.jsx";
import DistanceMatrix from "./components/Examples/googlemaps/distanceMatrix/DistanceMatrix.jsx";
import DistanceMatrix2 from "./components/Examples/googlemaps/distanceMatrix/DistanceMatrixTwo.jsx";
import BasicMapOne from "./components/Examples/googlemaps/basicmaps/BasicMapOne.tsx";
import { MapVisgl } from "./components/Examples/googlemaps/basicmaps/BasicMapTwoVisGL.tsx";
import DirectionVis from "./components/Examples/googlemaps/DirectionsVisGl/DirectionVis.tsx";
import GoogleMapsNext from "./components/Examples/googlemaps/NextJsgoogleMap/GoogleMapsNext.jsx";
import GoogleMapRouteComponent from "./components/Examples/googlemaps/NextJsgoogleMap/GoogleMapRouteComponent.jsx";
import MultipleMarkersMap from "./components/Examples/googlemaps/NextJsgoogleMap/MultiMarkers.jsx";
import SchedulerBitnoise from "./components/Examples/calendars-schedulers/SchedulerBitnoise.jsx";
import AldabilScheduler from "./components/Examples/calendars-schedulers/AldabilScheduler/AldabilScheduler.jsx";
//import AldabilSchedulerTwo from "./components/Examples/calendars-schedulers/AldabilScheduler/AldabilSchedulerTwo.tsx";

MapVisgl;
function App() {
  return (
    <>
      <header className="header">
        <h3>Testing some React libraries here</h3>
        <h3>& Bookmarks saved</h3>
      </header>

      <Navigation />

      <main id="main-content">
        <Routes>
          <Route
            path="momentjs/duration/calendar"
            element={<MomentDurationCalendar />}
          />
          <Route path="googlemaps/basicmap1" element={<BasicMapOne />} />
          <Route path="googlemaps/basicmap2" element={<MapVisgl />} />
          <Route
            path="googlemaps/distancematrix"
            element={<DistanceMatrix />}
          />
          <Route
            path="googlemaps/distancematrix2"
            element={<DistanceMatrix2 />}
          />
          <Route
            path="googlemaps/directionsvisgl1"
            element={<DirectionVis />}
          />
          <Route path="googlemaps/nextjs" element={<GoogleMapsNext />} />

          <Route
            path="googlemaps/nextjs2"
            element={<GoogleMapRouteComponent />}
          />

          <Route path="googlemaps/nextjs3" element={<MultipleMarkersMap />} />

          <Route
            path="calendars-scheduler/bitnoise.pl"
            element={<SchedulerBitnoise />}
          />

          <Route
            path="calendars-scheduler/react-big-calendar-mpersson"
            element={<IframeComponent src="https://reactbigcalendar-6nf68p5w.b4a.run" />}
          />

          <Route
            path="calendars-scheduler/aldabil-example"
            element={<AldabilScheduler />}
          />
          {/*  <Route
            path="calendars-scheduler/aldabil-example2"
            element={<AldabilSchedulerTwo />}
          /> */}

          <Route
            path="allbookmarks"
            element={
              <IframeComponent src="https://raindrop.io/mitfreex/front-end-44984325/embed" />
            }
          />
          <Route
            path="primereact/icons"
            element={<IframeComponent src="https://primereact.org/icons/" />}
          />
          <Route
            path="primereact/calendar"
            element={<IframeComponent src="https://primereact.org/calendar/" />}
          />
          <Route
            path="primereact/megamenu"
            element={<IframeComponent src="https://primereact.org/megamenu" />}
          />
        </Routes>
      </main>
    </>
  );
}

export default App;

/* import { Outlet, Route, Routes, useNavigate } from "react-router-dom";
import "./App.css";
//import Navigation from "./components/navigation";
import DirectionVis from './components/Examples/googlemaps/DirectionsVisGl/DirectionVis';
import { APIProvider } from '@vis.gl/react-google-maps';
import GoogleMapsNext from './components/Examples/googlemaps/NextJsgoogleMap/GoogleMapsNext';

function Dashboard() {
  return (
    <div>
      <h1>Dashboard</h1>

      
      <Outlet />
    </div>
  );
}

function App() {
  return (
    <Routes>
      <Route path="/dashboard" element={<Dashboard />}>
        <Route path="messages" element={<h3>DashboardMessages</h3>} />
        <Route path="tasks" element={<h3>DashboardTasks </h3>} />
      </Route>
    </Routes>
  );
}

export default App;
 */
