import React from 'react';
import { createRoot } from 'react-dom/client';
import { APIProvider, Map } from '@vis.gl/react-google-maps';
const API_KEY = globalThis.GOOGLE_MAPS_API_KEY ?? (import.meta.env.VITE_REACT_APP_GOOGLE_MAPS_API_KEY as string);


export const MapVisgl = () => {


    return (
        <>
            <h4>@vis.gl/react-google-maps</h4>
            <APIProvider apiKey={API_KEY}>
                <Map
                    style={{ width: '50vw', height: '50vh' }}
                    defaultCenter={{
                        lat: 42.699367,
                        lng: 23.313931
                    }}
                    defaultZoom={15}
                    gestureHandling={'greedy'}
                    disableDefaultUI={false}
                />
            </APIProvider>
        </>

    );
}