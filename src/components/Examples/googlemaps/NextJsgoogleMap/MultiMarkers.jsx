"use client";
import React from "react";
import { GoogleMap, LoadScript, Marker } from "@react-google-maps/api";

const API_KEY =
  globalThis.GOOGLE_MAPS_API_KEY ??
  import.meta.env.VITE_REACT_APP_GOOGLE_MAPS_API_KEY;

const containerStyle = {
  width: "100%",
  height: "400px",
};

const center = {
  lat: 37.437041393899676,
  lng: -4.191635586788259,
};

const locations = [
  { lat: 37.43704149389968, lng: -4.191635586788259 },
  { lat: 37.43704149389968, lng: -4.171635586788259 },
  // Add more locations here
];

const MultipleMarkersMap = () => {
  return (
    <>
    <br /><br />
    <h4>Multimarkers</h4>
    <LoadScript googleMapsApiKey={API_KEY}>
      <GoogleMap mapContainerStyle={containerStyle} center={center} zoom={15}>
        {locations.map((location, index) => (
          <Marker key={index} position={location} />
          
        ))}

       
      </GoogleMap>
    </LoadScript>
    </>
    
  );
};

export default MultipleMarkersMap;

/* 
const containerStyle = {
  width: "100%",
  height: "400px",
};

const center = {
  lat: 37.437041393899676,
  lng: -4.191635586788259,
};

export default function GoogleMapsNext() {
  return (
    <>
      <h4>Google Maps API in Next.js</h4>

      <LoadScript googleMapsApiKey={API_KEY}>
        <GoogleMap mapContainerStyle={containerStyle} center={center} zoom={10}>
          <Marker position={center} />
        </GoogleMap>
      </LoadScript>
    </>
  );
}
  */
