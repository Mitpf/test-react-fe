import React from "react";
import { GoogleMap, LoadScript, Marker } from "@react-google-maps/api";
const API_KEY =
  globalThis.GOOGLE_MAPS_API_KEY ??
  import.meta.env.VITE_REACT_APP_GOOGLE_MAPS_API_KEY;

const containerStyle = {
  width: "100%",
  height: "400px",
};

const center = {
  lat: 37.437041393899676,
  lng: -4.191635586788259,
};

export default function GoogleMapsNext() {
  return (
    <>
      <h4>Google Maps API in Next.js</h4>

      <LoadScript googleMapsApiKey={API_KEY}>
        <GoogleMap mapContainerStyle={containerStyle} center={center} zoom={10}>
          <Marker position={center} />
        </GoogleMap>
      </LoadScript>
    </>
  );
}
