import React, { useEffect } from "react";

import styles from "./styles.module.css";

const jsxCode = `
const DistanceMatrix = () => {
  useEffect(() => {
    const script = document.createElement("script");
    script.src = \`https://maps.googleapis.com/maps/api/js?key=AIzaSyAplfiYlBRluxlwtEjO8D17vAFulHjqjDE&callback=initMap&v=weekly\`;
    script.defer = true;
    document.head.appendChild(script);

    script.onload = () => {
      initMap();
    };

    const initMap = () => {
      const bounds = new google.maps.LatLngBounds();
      const markersArray = [];
      const map = new google.maps.Map(document.getElementById("map"), {
        center: { lat: 55.53, lng: 9.4 },
        zoom: 10,
      });
      const geocoder = new google.maps.Geocoder();
      const service = new google.maps.DistanceMatrixService();
      const origin3 = { lat: 42.1432, lng: 24.742447 };
      const destinationC = "Sofia, Bulgaria";
      const request = {
        origins: [origin3],
        destinations: [destinationC],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false,
      };

      document.getElementById("request").innerText = JSON.stringify(
        request,
        null,
        2
      );

      service.getDistanceMatrix(request).then((response) => {
        document.getElementById("response").innerText = JSON.stringify(
          response,
          null,
          2
        );

        const originList = response.originAddresses;
        const destinationList = response.destinationAddresses;

        deleteMarkers(markersArray);

        const showGeocodedAddressOnMap = (asDestination) => {
          const handler = ({ results }) => {
            map.fitBounds(bounds.extend(results[0].geometry.location));
            markersArray.push(
              new google.maps.Marker({
                map,
                position: results[0].geometry.location,
                label: asDestination ? "D" : "O",
              })
            );
          };
          return handler;
        };

        for (let i = 0; i < originList.length; i++) {
          const results = response.rows[i].elements;

          geocoder
            .geocode({ address: originList[i] })
            .then(showGeocodedAddressOnMap(false));

          for (let j = 0; j < results.length; j++) {
            geocoder
              .geocode({ address: destinationList[j] })
              .then(showGeocodedAddressOnMap(true));
          }
        }
      });
    };

    const deleteMarkers = (markersArray) => {
      for (let i = 0; i < markersArray.length; i++) {
        markersArray[i].setMap(null);
      }

      markersArray = [];
    };

    return () => {
      document.head.removeChild(script);
    };
  }, []);

  return (
    <div className={styles.container}>
      <div id="map" className={styles.map}></div>
      <div id="sidebar" className={styles.sidebar}>
        <h3 style={{ flexGrow: 0 }}>Request</h3>
        <pre style={{ flexGrow: 1 }} id="request"></pre>
        <h3 style={{ flexGrow: 0 }}>Response</h3>
        <pre style={{ flexGrow: 1 }} id="response"></pre>
      </div>
    </div>
  );
};

export default DistanceMatrix;
  `;

const DistanceMatrix = () => {
  useEffect(() => {
    const script = document.createElement("script");
    script.src = `https://maps.googleapis.com/maps/api/js?key=AIzaSyAplfiYlBRluxlwtEjO8D17vAFulHjqjDE&callback=initMap&v=weekly`;
    script.defer = true;
    document.head.appendChild(script);

    script.onload = () => {
      initMap();
    };

    const initMap = () => {
      const bounds = new google.maps.LatLngBounds();
      const markersArray = [];
      const map = new google.maps.Map(document.getElementById("map"), {
        center: { lat: 55.53, lng: 9.4 },
        zoom: 10,
      });
      const geocoder = new google.maps.Geocoder();
      const service = new google.maps.DistanceMatrixService();
      const origin3 = { lat: 42.1432, lng: 24.742447 };
      const destinationC = "Sofia, Bulgaria";
      const request = {
        origins: [origin3],
        destinations: [destinationC],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false,
      };

      document.getElementById("request").innerText = JSON.stringify(
        request,
        null,
        2
      );

      service.getDistanceMatrix(request).then((response) => {
        document.getElementById("response").innerText = JSON.stringify(
          response,
          null,
          2
        );

        const originList = response.originAddresses;
        const destinationList = response.destinationAddresses;

        deleteMarkers(markersArray);

        const showGeocodedAddressOnMap = (asDestination) => {
          const handler = ({ results }) => {
            map.fitBounds(bounds.extend(results[0].geometry.location));
            markersArray.push(
              new google.maps.Marker({
                map,
                position: results[0].geometry.location,
                label: asDestination ? "D" : "O",
              })
            );
          };
          return handler;
        };

        for (let i = 0; i < originList.length; i++) {
          const results = response.rows[i].elements;

          geocoder
            .geocode({ address: originList[i] })
            .then(showGeocodedAddressOnMap(false));

          for (let j = 0; j < results.length; j++) {
            geocoder
              .geocode({ address: destinationList[j] })
              .then(showGeocodedAddressOnMap(true));
          }
        }
      });
    };

    const deleteMarkers = (markersArray) => {
      for (let i = 0; i < markersArray.length; i++) {
        markersArray[i].setMap(null);
      }

      markersArray = [];
    };

    return () => {
      document.head.removeChild(script);
    };
  }, []);

  return (
    <>
    <h4>vanilla JS distance matrix v1</h4>
      <div className={styles.container}>
        <div id="map" className={styles.map}></div>
        <div id="sidebar" className={styles.sidebar}>
          <h3 style={{ flexGrow: 0 }}>Request</h3>
          <pre style={{ flexGrow: 1 }} id="request"></pre>
          <h3 style={{ flexGrow: 0 }}>Response</h3>
          <pre style={{ flexGrow: 1 }} id="response"></pre>
        </div>
      </div>
      <h4>CODE:</h4>
      <div className={styles.map}>
        <pre>
          <code dangerouslySetInnerHTML={{ __html: jsxCode }} />
        </pre>
      </div>
    </>
  );
};

export default DistanceMatrix;
