
/* TSX */



import React, { useEffect, useState } from 'react';
import Directions from '../DirectionsVisGl/directionsMaps/Directions';
import styles from './styles.module.css';

import {
  APIProvider,
  Map,
  useMapsLibrary,
  useMap
} from '@vis.gl/react-google-maps';
import { log } from 'console';
import { useHref } from 'react-router-dom';
import classNames from 'classnames';

const API_KEY = globalThis.GOOGLE_MAPS_API_KEY ?? (import.meta.env.VITE_REACT_APP_GOOGLE_MAPS_API_KEY as string);

type WayPoint = {
  location: string;
  stopover: boolean;
}

const DirectionVis = () => {

  const [points, setPoints] = useState({ start: '', destination: '' });
  const [inputValues, setInputvalues] = useState({ start: '', destination: '', waypts: ['Plovdiv'] });
  const [wayPoints, setWayPoints] = useState<WayPoint[]>([]);
  const [inputWayPointValue, setInputWayPointValue] = useState('');

  function onInpChangeHandler(e) {

    setInputvalues(state => ({ ...inputValues, [e.target.name]: e.target.value }))
  }

  function onInpWPChangeHandler(e) {
    if (e.target.value.trim()) {
      setInputWayPointValue(e.target.value);
    }

  }

  function submitInputs() {
   // console.log('sadfasd')
    setPoints(state => ({ ...points, ...inputValues }))
  }

  function submitWPInputs() {
    if (inputWayPointValue) {
      setWayPoints((state) => ([...state, { location: inputWayPointValue, stopover: true }]));
      setInputWayPointValue('');
    }
  }

  function handleKeyPress(e) {
    if (e.key === 'Enter') {
      submitWPInputs()
    }
  };

  function onMinusPoint(index) {
    setWayPoints(state => [...wayPoints].filter((x, i) => i !== index))
  }

  return (
    <>
    <h4></h4>
      <div className={styles.inputsContainer}>
        <label htmlFor="start">Start point: </label>
        <input className={styles.generalInputs} type="text" name="start" onChange={onInpChangeHandler} value={inputValues.start} />
        <br /> <br />

        <a className={styles.ancButton} onClick={submitWPInputs}>+ add way points </a>

        <input type="text" className={classNames(styles.wpinput, styles.generalInputs)}
          name="waypoint" value={inputWayPointValue} onChange={onInpWPChangeHandler} onKeyDown={handleKeyPress} />

        <div className={styles.wayPointsList}>
          {wayPoints.length > 0 &&
            wayPoints.map((value, index) => (
              <div id={index.toString()}
                key={index + value.location + value.location.length}
                className={styles.waypointsP}> {value.location}
                <a className={classNames(styles.ancButton, styles.minusBtn)} onClick={() => onMinusPoint(index)}>-</a></div>
            ))}
        </div>


        <div>
          <label htmlFor="destination" className={styles.label}>Final Destination: </label>
          <input className={classNames(styles.generalInputs)} type="text" name="destination" onChange={onInpChangeHandler} value={inputValues.destination} />
          <button className={styles.btn} onClick={submitInputs}>Submit</button></div>

        <br /><br />
      </div>

      < APIProvider apiKey={API_KEY} >
        <Map
          style={{ width: '80vw', height: '50vh' }}
          
          defaultCenter={{ lat: 42.696, lng: 23.317 }}
          defaultZoom={16}
          gestureHandling={'greedy'}
          fullscreenControl={false}
        >
          <Directions start={points.start} destination={points.destination} waypts={wayPoints} />
        </Map>
      </APIProvider >
    </>

  );
}



export default DirectionVis;



