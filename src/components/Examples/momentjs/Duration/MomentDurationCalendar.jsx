import { useEffect, useState } from "react";
import useDuration from "./hooks/useDuration.jsx";
import CalendarDuration from "./components/CalendarDuration.jsx";
import { MemoizedShowduration } from "./components/ShowDuration.jsx";
import moment from "moment";

function MomentDurationCalendar() {
  const [apiDate, setApiDate] = useState(null);

  const date = new Date(apiDate);

  const formattedDate = date.toLocaleString('bg-BG', {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
  });

  useEffect(() => {
    const url = `https://swapi.dev/api/people`;

    fetch(url)
      .then((res) => res.json())
      .then((data) => {
        console.log(data.results[5].created);

        setApiDate(data.results[5].created);
      });
  }, []);

  const [getDuration] = useDuration();

  return (
    <>
      <h2>prime react calendar + moment js lib</h2>
      <h4>using hook state interval for current time </h4>
      <br />
      <CalendarDuration />
      <br /><br />
      <CalendarDuration />
      <br /><br />

      <h4>some date fetched from swapi {formattedDate} or {date.toDateString()}</h4>
      <p>time from now: {getDuration(apiDate)}</p>

      <h4>not using hook useduration here</h4>

<h3>here memoized</h3>
      <MemoizedShowduration apiDate={apiDate} />      
    </>
  );
}

export default MomentDurationCalendar;