import moment from "moment";
import { memo } from "react";

function ShowDuration({ apiDate }) {
  const date = new Date(apiDate);

  // format
  const formattedDate = `${date.getDate()}-${
    date.getMonth() + 1
  }-${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;

  const duration = moment.duration(moment() - moment(apiDate));

  return (
    <>
      <p>created at (just formatiing fetched data): {formattedDate}</p>
      <p>
        time from now: {duration.years()}years {duration.months()}months{" "}
        {duration.weeks()}weeks {duration.days()}days {duration.hours()}hours{" "}
        {duration.minutes()}minutes {duration.seconds()}seconds
      </p>
      <p>
        time now {moment().format("MMMM Do YYYY h:mm:ss a")}{" "}
        {new Date().toString()}
      </p>
    </>
  );
}

export const MemoizedShowduration = memo(ShowDuration);

