import { useState } from "react";

import useDuration from "../hooks/useDuration.jsx";
import { Calendar } from "primereact/calendar";

export default function CalendarDuration() {
  const [date, setDate] = useState(new Date());
  function onChangeHandler(e) {
    setDate(e.value);
  }
  const [getDurationFromNow] = useDuration();

  return (
    <>
      <p> {getDurationFromNow(date)}</p>
      <Calendar showIcon showTime value={date} onChange={onChangeHandler} />
    </>
  );
}
