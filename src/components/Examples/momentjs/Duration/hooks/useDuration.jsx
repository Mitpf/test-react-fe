/* USAGE  */

import { useState, useEffect } from "react";
import moment from "moment";
export default function useDuration() {
  const [nowTime, setNowTime] = useState(new Date());

  useEffect(() => {
    const interval = setInterval(() => setNowTime(new Date()), 1000);

    // clear interval on unmount
    return () => clearInterval(interval);
  }, []);

  function getDuration(targetDate, options, refDate) {
    if (!targetDate) {
      return "no valid date";
    }

    // ref or just now
    const ref = moment(refDate);

    const isFuture = moment(targetDate).isAfter(ref);
    let duration = moment.duration(ref.diff(targetDate));

    //console.log('DURAtion',duration.weeks());

    let durMes = {
      years: { qt: duration.years() },
      months: { qt: duration.months() },
      weeks: { qt: duration.weeks() },
      days: { qt: duration.days() },
      hours: { qt: duration.hours() },
      minutes: { qt: duration.minutes() },
      seconds: { qt: duration.seconds() },
      //milliseconds: { qt: duration.milliseconds() },
    };

    Object.keys(durMes).forEach((key) => {
      // absolute values
      durMes[key].qt = Math.abs(durMes[key].qt);

      // add property uniteMes for name of unite
      // check unit measurment plural or singular day or days  ...
      if (durMes[key].qt > 1) {
        durMes[key].uniteMes = key.toString();
      } else {
        durMes[key].uniteMes = key.toString().slice(0, -1);
      }
    });

    if (options) {
      let newDurations = {};

      options.forEach((key) => {
        if (Object.keys(durMes).includes(key)) {
          newDurations = { ...newDurations, [key]: durMes[key] };
        }
      });

      durMes = newDurations;
    }

    let finalMessage = Object.keys(durMes).reduce((acc, key) => {
      if (durMes[key].qt !== 0) {
        acc += `${durMes[key].qt} ${durMes[key].uniteMes} `;
      }
      return acc;
    }, "");

    if (isFuture) {
      return `remaining time in ${finalMessage}`;
    } else {
      return `elapsed time ${finalMessage} ago`;
    }
  }

  const getDurationFromNow = (date, options) =>
    getDuration(date, options, nowTime);
  return [getDurationFromNow, nowTime];
}
