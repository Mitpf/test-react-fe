
export const moreMapsItems = [
    {
        label: "Mapbox GL JS",
        items: [
            {
                label: "Mapbox documentation",
                icon: "pi pi-external-link",
                command: () => {
                    window.open(
                        "https://docs.mapbox.com/mapbox-gl-js/guides",
                        "_blank"
                    );
                },
            },
            {
                label: "Matrix API",
                icon: "pi pi-external-link ",
                command: () => {
                    window.open(
                        "https://docs.mapbox.com/api/navigation/matrix/",
                        "_blank"
                    );
                },
            },
            {
                label: "Optimization API v1",
                icon: "pi pi-external-link ",
                command: () => {
                    window.open(
                        "https://docs.mapbox.com/api/navigation/optimization-v1/",
                        "_blank"
                    );
                },
            },
            {
                label: "--> Mapbox examples",
                icon: "pi pi-external-link",
                command: () => {
                    window.open(
                        "https://docs.mapbox.com/mapbox-gl-js/example/",
                        "_blank"
                    );
                },
            },
            {
                label: "--> Example Display navigation directions",
                icon: "pi pi-external-link ",
                command: () => {
                    window.open(
                        "https://docs.mapbox.com/mapbox-gl-js/example/mapbox-gl-directions/",
                        "_blank"
                    );
                },
            },
        ],
    },
    [
        {
            label: "Map Libre",

            items: [
                {
                    label: "--> examples",
                    icon: "pi pi-external-link",
                    command: () => {
                        window.open(
                            "https://maplibre.org/maplibre-gl-js/docs/examples/",
                            "_blank"
                        );
                    },
                },
            ],
        },
        {
            label: "mapquest.com- routes optimizing",
            items: [
                {
                    label: ".mapquest.com/directions",
                    icon: "pi pi-external-link",
                    command: () => {
                        window.open("https://www.mapquest.com/directions", "_blank");
                    },
                },
            ],
        },
        {
            label: "routific.com-routes optimizing",
            items: [
                {
                    label: ".mapquest.com/directions",
                    icon: "pi pi-external-link",
                    command: () => {
                        window.open("https://www.routific.com/", "_blank");
                    },
                },
            ],
        },
    ],
]