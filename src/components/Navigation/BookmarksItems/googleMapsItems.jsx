export const googleMapsItems = [
  [
    {
      label: " JS examples & docs",
      items: [
        {
          label: "--> Examples",

          icon: "pi pi-external-link",
          command: () => {
            window.open(
              "https://developers.google.com/maps/documentation/javascript/examples",
              "_blank"
            );
          },
        },
        {
          label: "Routes API",

          icon: "pi pi-external-link",
          command: () => {
            window.open(
              "https://developers.google.com/maps/documentation/routes",
              "_blank"
            );
          },
        },
        {
          label: "Directions API",

          icon: "pi pi-external-link",
          command: () => {
            window.open(
              "https://developers.google.com/maps/documentation/directions",
              "_blank"
            );
          },
        },
        {
          label: "routeoptimization API client for Node.js",

          icon: "pi pi-github",
          command: () => {
            window.open(
              "https://github.com/googleapis/google-cloud-node/tree/main/packages/google-maps-routeoptimization",
              "_blank"
            );
          },
        },
        {
          label: "--> JS example demo example Geocoding Service",

          icon: "pi pi-external-link",
          command: () => {
            window.open(
              "https://developers.google.com/maps/documentation/javascript/examples/geocoding-simple",
              "_blank"
            );
          },
        },
        {
          label: "--> JS example demo Directions Service",

          icon: "pi pi-external-link",
          command: () => {
            window.open(
              "https://developers.google.com/maps/documentation/javascript/examples/directions-simple",
              "_blank"
            );
          },
        },
        {
          label: "--> JS example demo Waypoints in Directions",

          icon: "pi pi-external-link",
          command: () => {
            window.open(
              "https://developers.google.com/maps/documentation/javascript/examples/directions-waypoints",
              "_blank"
            );
          },
        },
        {
          label: "--> JS example demo Distance Matrix Service",

          icon: "pi pi-external-link",
          command: () => {
            window.open(
              "https://developers.google.com/maps/documentation/javascript/examples/distance-matrix",
              "_blank"
            );
          },
        },
      ],
    },
  ],
  [
    {
      label: "googleMaps in React Next.js",
      items: [
        {
          label: "creating Map with Google Maps API in next.js",
          icon: "pi pi-external-link",
          command: () => {
            window.open(
              "https://dev.to/adrianbailador/creating-an-interactive-map-with-the-google-maps-api-in-nextjs-54a4",
              "_blank"
            );
          },
        },
      ],
    },
    {
      label: "Vis Gl docs/ REACT demos",

      items: [
        {
          label: "Vis Gl installation in cmd/shell ",
          icon: "pi pi-external-link",
          command: () => {
            window.open(
              "https://visgl.github.io/react-google-maps/docs/get-started#installation",
              "_blank"
            );
          },
        },
        {
          label: "--> React example demo Vis Gl basic Map ",
          icon: "pi pi-external-link",
          command: () => {
            window.open(
              "https://visgl.github.io/react-google-maps/examples/basic-map",
              "_blank"
            );
          },
        },
        {
          label: "Code React example demo Vis Gl basic Map ",
          icon: "pi pi-github",
          command: () => {
            window.open(
              "https://github.com/visgl/react-google-maps/tree/main/examples/basic-map",
              "_blank"
            );
          },
        },
        {
          label: "--> React example demo Vis Gl Directions",
          icon: "pi pi-external-link",
          command: () => {
            window.open(
              "https://visgl.github.io/react-google-maps/examples/directions"
            );
          },
        },
        {
          label: "Code React example demo Vis Gl Directions",
          icon: "pi pi-github",
          command: () => {
            window.open(
              "https://github.com/visgl/react-google-maps/tree/main/examples/directions"
            );
          },
        },
      ],
    },
  ],
];
