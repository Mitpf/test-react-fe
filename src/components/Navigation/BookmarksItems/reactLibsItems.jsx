export const reactLibsItems = [
  [
    {
      label: "npm@syncfusion",
      items: [
        {
          label: "Kanban",
          command: () => {
            window.open(
              "https://www.npmjs.com/package/@syncfusion/ej2-react-kanban",
              "_blank"
            );
          },
          icon: "pi pi-external-link",
        },
        {
          label: "--> Demo Kanban",
          command: () => {
            window.open(
              "https://ej2.syncfusion.com/react/demos/?utm_source=npm&utm_medium=listing&utm_campaign=react-kanban-npm#/bootstrap5/kanban/overview",
              "_blank"
            );
          },
          icon: "pi pi-external-link",
        },
        {
          label: "Calendars",
          command: () => {
            window.open(
              "https://www.npmjs.com/package/@syncfusion/ej2-react-calendars",
              "_blank"
            );
          },
          icon: "pi pi-external-link",
        },
        {
          label: "--> Demo Calendars",
          command: () => {
            window.open(
              "https://ej2.syncfusion.com/react/demos/?utm_source=npm&utm_medium=listing&utm_campaign=react-calendar-npm#/bootstrap5/calendar/multi-selection",
              "_blank"
            );
          },
          icon: "pi pi-external-link",
        },
        {
          label: "Schedule",
          command: () => {
            window.open(
              "https://www.npmjs.com/package/@syncfusion/ej2-react-schedule",
              "_blank"
            );
          },
          icon: "pi pi-external-link",
        },
        {
          label: "--> Demo Schedule",
          command: () => {
            window.open(
              "https://ej2.syncfusion.com/react/demos/?utm_source=npm&utm_medium=listing&utm_campaign=react-scheduler-npm#/bootstrap5/schedule/overview",
              "_blank"
            );
          },
          icon: "pi pi-external-link",
        },
        {
          label: "Data Grid",
          command: () => {
            window.open(
              "https://www.npmjs.com/package/@syncfusion/ej2-react-grids",
              "_blank"
            );
          },
          icon: "pi pi-external-link",
        },
        {
          label: "--> Demo Data Grid",
          command: () => {
            window.open(
              "https://ej2.syncfusion.com/react/demos/?utm_source=npm&utm_medium=listing&utm_campaign=react-grid-npm#/bootstrap5/grid/live-data",
              "_blank"
            );
          },
          icon: "pi pi-external-link",
        },
      ],
    },
    {
      label: "Design",
      items: [
        {
          label: "primereact.org",

          command: () => {
            window.open("https://primereact.org", "_blank");
          },
          icon: "pi pi-external-link",
        },
        {
          label: "ui.mantine.dev",

          command: () => {
            window.open("https://ui.mantine.dev/", "_blank");
          },
          icon: "pi pi-external-link",
        },
        {
          label: "coolors.co",

          command: () => {
            window.open("https://coolors.co/", "_blank");
          },
          icon: "pi pi-external-link",
        },
        {
          label: "▶️ Hyperplexed",
          command: () => {
            window.open(
              "https://www.youtube.com/@Hyperplexed/videos",
              "_blank"
            );
          },
          icon: "pi pi-external-link",
        },
      ],
    },
  ],
  [
    {
      label: "npm react-Google maps",
      items: [
        {
          label: "@react-google-maps/api",

          command: () => {
            window.open(
              "https://www.npmjs.com/package/@react-google-maps/api",
              "_blank"
            );
          },
          icon: "pi pi-external-link",
        },
        {
          label: "@vis.gl/react-google-maps",

          command: () => {
            window.open(
              "https://www.npmjs.com/package/@vis.gl/react-google-maps?activeTab=readme",
              "_blank"
            );
          },
          icon: "pi pi-external-link",
        },
        {
          label: "ADD VIS GL in REACT",

          command: () => {
            window.open(
              "https://developers.google.com/codelabs/maps-platform/maps-platform-101-react-js#0",
              "_blank"
            );
          },
          icon: "pi pi-external-link",
        },
        {
          label: "vis gl documentation",

          command: () => {
            window.open(
              "https://visgl.github.io/react-google-maps/docs/get-started#installation",
              "_blank"
            );
          },
          icon: "pi pi-external-link",
        },
        {
          label: "react-map-gl for Mapbox GL JS-compatible ..",

          command: () => {
            window.open("https://www.npmjs.com/package/react-map-gl", "_blank");
          },
          icon: "pi pi-external-link",
        },
        {
          label: "Google Maps API for Node.js",

          command: () => {
            window.open("https://www.npmjs.com/package/googlemaps", "_blank");
          },
          icon: "pi pi-external-link",
        },
      ],
    },
  ],
  [
    {
      label: "Callendars",

      items: [
        {
          label: "--> fullcalendar demos",
          icon: "pi pi-external-link",
          command: () => {
            window.open("https://fullcalendar.io/demos", "_blank");
          },
        },
        {
          label: "fullcalendar docs",
          icon: "pi pi-external-link",
          command: () => {
            window.open("https://fullcalendar.io/docs/react", "_blank");
          },
        },
        {
          label: "Article Calendar with MUI material + other tools by M. Persson",
          icon: "pi pi-external-link",
          command: () => {
            window.open("https://dev.to/martinpersson/react-material-ui-create-a-dynamic-calendar-with-integrated-todos-6cm", "_blank");
          },
        },
        {
          label: "--> demo Calendar with MUI +tools by M. Persson",
          icon: "pi pi-external-link",
          command: () => {
            window.open("https://react-calendar-example.vercel.app/", "_blank");
          },
        },
        {
          label: "aldabil21 Material-UI react-scheduler",
          icon: "pi pi-github",
          command: () => {
            window.open("https://github.com/aldabil21/react-scheduler", "_blank");
          },
        },
      ],
    },
  ],
];
