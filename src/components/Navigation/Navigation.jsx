import "primeicons/primeicons.css";
import { MegaMenu } from "primereact/megamenu";
import { Link, useNavigate } from "react-router-dom";
import { moreMapsItems } from "./BookmarksItems/moreMapsItems.jsx";
import { googleMapsItems } from "./BookmarksItems/googleMapsItems.jsx";
import { reactLibsItems } from "./BookmarksItems/reactLibsItems";

export default function Navigation() {
  const navigate = useNavigate();
  const items = [
    {
      label: "Examples",
      icon: "pi pi-bullseye",
      items: [
        [
          {
            label: "ReactCalendars/schedulers",
            items: [
              { label: "react-big-calendarM.Persson",
                icon: "pi pi-bullseye",
                command:()=>navigate('calendars-scheduler/react-big-calendar-mpersson')
               },
              {
                label: "scheduler.bitnoise.pl",
                icon: "pi pi-bullseye",
                command: () => navigate("calendars-scheduler/bitnoise.pl"),
              },
              {
                label: "example @aldabil/react-scheduler M UI",
                icon: "pi pi-bullseye",
                command: () => navigate("calendars-scheduler/aldabil-example"),
              },
            ],
          },
          {
            label: "Moment JS",
            items: [
              {
                label: "Calendar Duration",
                icon: "pi pi-bullseye",
                command: () => {
                  navigate("momentjs/duration/calendar");
                },
              },
            ],
          },
          {
            label: "Google Maps examples",
            items: [
              {
                label: "Basic map1",
                icon: "pi pi-bullseye",
                command: () => navigate("googlemaps/basicmap1"),
              },
              {
                label: "Basic map2 visgl",
                icon: "pi pi-bullseye",
                command: () => navigate("googlemaps/basicmap2"),
              },
              {
                label: "Distance Matrix vanilla js to jsx",
                icon: "pi pi-bullseye",
                command: () => navigate("googlemaps/distancematrix"),
              },
              {
                label: "Distance Matrix vanilla js to jsx 2",
                icon: "pi pi-bullseye",
                command: () => navigate("googlemaps/distancematrix2"),
              },
              {
                label: "Directions visgl",
                icon: "pi pi-bullseye",
                command: () => navigate("googlemaps/directionsvisgl1"),
              },
            ],
          },
        ],
        [
          {
            label: "Article React google-maps API in Next JS",
            items: [
              {
                label: "example 1 Next js googlemaps",
                icon: "pi pi-bullseye",
                command: () => navigate("googlemaps/nextjs"),
              },
              {
                label: "example 2 Next js googlemaps",
                icon: "pi pi-bullseye",
                command: () => navigate("googlemaps/nextjs2"),
              },
              {
                label: "example 3- Multimarkers Next js googlemaps",
                icon: "pi pi-bullseye",
                command: () => navigate("googlemaps/nextjs3"),
              },
              {
                label: "example 4 Next js googlemaps",
                icon: "pi pi-bullseye",
                command: () => navigate("googlemaps/nextjs2"),
              },
            ],
          },
        ],
      ],
    },
    {
      label: "PrimeReact",
      icon: "pi pi-prime",
      items: [
        [
          {
            label: "Components",

            items: [
              {
                label: "MegaMenu",
                icon: "pi pi-link",
                command: () => {
                  navigate("primereact/megamenu");
                },
              },
              {
                label: "Calendar",
                icon: "pi pi-link",
                command: () => navigate("primereact/calendar"),
              },
            ],
          },
        ],
        [
          {
            label: "PrimeReact Icons",
            items: [
              {
                label: "Icons PrimeReact",
                icon: "pi pi-link",
                command: () => {
                  navigate("primereact/icons");
                },
              },
              {
                label: "primereact.org/icons",
                icon: "pi pi-external-link",
                command: () => {
                  window.open("https://primereact.org/icons/", "_blank");
                },
              },
            ],
          },
        ],
      ],
    },

    {
      label: "React Libs",
      icon: "pi pi-bookmark",
      items: reactLibsItems,
    },
    {
      label: "Google-maps API",
      icon: "pi pi-bookmark-fill",
      items: googleMapsItems,
    },
    {
      label: "more Maps",
      icon: "pi pi-bookmark",
      items: moreMapsItems,
    },
    {
      label: "All BookMarks",
      icon: "pi pi-bookmark-fill",
      command: () => {
        window.open(
          "https://raindrop.io/mitfreex/weclean-project-44984438",
          "_blank"
        );
      },
    },
  ];

  return (
    <>
      <MegaMenu model={items} breakpoint="960px" />
    </>
  );
}
