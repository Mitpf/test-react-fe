import React from "react";

const IframeComponent = ({ src, classname }) => {
  return (
    <div style={{ width: "100%", height: "500px", border: "1px solid #ccc", marginTop:"4em" }} className={classname }>
      <iframe
        src={src}
        style={{ width: "100%", height: "100%" }}
        
        frameBorder="0"
      ></iframe>
    </div>
  );
};

export default IframeComponent;
